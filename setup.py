import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tclib",
    version="0.0.1",
    author="Abhinandan Panigrahi",
    author_email="abhi@edyst.com",
    description="Edyst Test Case Generator",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/edyst2/edyst-tclib/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
