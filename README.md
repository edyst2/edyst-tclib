# Edyst test case generator library

## installation

```python3
pip install git+https://bitbucket.org/edyst2/edyst-tclib
```

## usage

Sample `tc.py`:

```python3
from tclib import Int, ArrInt, ArrIntInt, Str, ArrStr, ArrStrStr, writetc

### generators
# Int(min=-10, max=10)
# ArrInt(N=10, min=-9, max=9, distinct=True, sorted=True, desc=True, disorder=0.1)
# ArrIntInt(N=3, M=5, min=-9, max=9, distinct=True)
# Str(minlenstr=3, maxlenstr=5, distinctchars=False)
# ArrStr(N=10, distinct=True, sorted=True, desc=True, disorder=0.1, minlenstr=3, maxlenstr=5, distinctchars=False)
# ArrStrStr(N=4, M=5, distinct=True, minlenstr=3, maxlenstr=5, distinctchars=False)
###

small_tc = {}

small_tc[1] = [
    1,
    ArrInt(N=10, min=-9, max=9, distinct=True, sorted=True, desc=True, disorder=0.1),
]

large_tc = {}

# large_tc[1] = [
#     1,
#     INSERT_HERE,
# ]

# large_tc[2] = [
#     1,
#     INSERT_HERE,
# ]

# large_tc[3] = [
#     1,
#     INSERT_HERE,
# ]

# large_tc[4] = [
#     1,
#     INSERT_HERE,
# ]

# large_tc[5] = [
#     1,
#     INSERT_HERE,
# ]

writetc(small_tc, "small")
writetc(large_tc, "large")
```
