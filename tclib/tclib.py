import random
import itertools
from pprint import pprint
import string

# constants
INT_MIN = -(2 ** 24)
INT_MAX = 2 ** 24

STRCHARSET = string.ascii_letters + string.digits
STRMINLEN = 5
STRMAXLEN = 10

ARR_1D_LEN = 10
DISORDER = 0.0

ARR_2D_N = 5
ARR_2D_M = 5


class Int:
    def __init__(self, min=INT_MIN, max=INT_MAX):
        self.data = random.randint(min, max)

    def __str__(self):
        return str(self.data)

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


class ArrInt:
    def __init__(
        self,
        N=ARR_1D_LEN,
        min=INT_MIN,
        max=INT_MAX,
        distinct=False,
        sorted=False,
        desc=False,
        disorder=DISORDER,
    ):
        if distinct is True:
            self.data = random.sample(range(min, max + 1), k=N)
        else:
            self.data = random.choices(range(min, max + 1), k=N)

        if sorted is True:
            self.data.sort(reverse=desc)
            num_inversions = int(len(self.data) * disorder)
            for _ in range(num_inversions):
                i = random.randint(0, len(self.data) - 1)
                j = random.randint(0, len(self.data) - 1)
                self.data[i], self.data[j] = self.data[j], self.data[i]

    def __str__(self):
        ele = " ".join(map(str, self.data))
        return f"{len(self.data)} {ele}"

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


class ArrIntInt:
    def __init__(
        self, N=ARR_2D_N, M=ARR_2D_M, min=INT_MIN, max=INT_MAX, distinct=False
    ):
        if distinct is True:
            self.data = random.sample(range(min, max + 1), k=N * M)
        else:
            self.data = random.choices(range(min, max + 1), k=N * M)

        self.data = [list(row) for row in itertools.zip_longest(*[iter(self.data)] * M)]

    def __str__(self):
        rows = [" ".join(map(str, arr)) for arr in self.data]
        ele = " ".join(rows)
        return f"{len(self.data)} {len(self.data[0])} {ele}"

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


class Str:
    def __init__(
        self,
        minlenstr=STRMINLEN,
        maxlenstr=STRMAXLEN,
        charset=STRCHARSET,
        distinctchars=False,
    ):
        _len = random.randint(minlenstr, maxlenstr)
        if distinctchars is True:
            self.data = "".join(random.sample(charset, k=_len))
        else:
            self.data = "".join(random.choices(charset, k=_len))

    def __str__(self):
        return self.data

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


class ArrStr:
    def __init__(
        self,
        N=ARR_1D_LEN,
        distinct=False,
        sorted=False,
        desc=False,
        disorder=DISORDER,
        minlenstr=STRMINLEN,
        maxlenstr=STRMAXLEN,
        charset=STRCHARSET,
        distinctchars=False,
    ):
        if distinct is True:
            A = set()
            while len(A) != N:
                A.add(
                    Str(
                        minlenstr=minlenstr,
                        maxlenstr=maxlenstr,
                        charset=charset,
                        distinctchars=distinctchars,
                    ).data
                )
            self.data = list(A)
        else:
            self.data = [
                Str(
                    minlenstr=minlenstr,
                    maxlenstr=maxlenstr,
                    charset=charset,
                    distinctchars=distinctchars,
                ).data
                for _ in range(N)
            ]

        if sorted is True:
            self.data.sort(reverse=desc)
            num_inversions = int(len(self.data) * disorder)
            for _ in range(num_inversions):
                i = random.randint(0, len(self.data) - 1)
                j = random.randint(0, len(self.data) - 1)
                self.data[i], self.data[j] = self.data[j], self.data[i]

    def __str__(self):
        ele = " ".join(self.data)
        return f"{len(self.data)} {ele}"

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


class ArrStrStr:
    def __init__(
        self,
        N=ARR_2D_N,
        M=ARR_2D_M,
        distinct=False,
        minlenstr=STRMINLEN,
        maxlenstr=STRMAXLEN,
        charset=STRCHARSET,
        distinctchars=False,
    ):
        A = ArrStr(
            N=N * M,
            distinct=distinct,
            minlenstr=minlenstr,
            maxlenstr=maxlenstr,
            charset=charset,
            distinctchars=distinctchars,
        ).data
        self.data = [list(row) for row in itertools.zip_longest(*[iter(A)] * M)]

    def __str__(self):
        rows = [" ".join(arr) for arr in self.data]
        ele = " ".join(rows)
        return f"{len(self.data)} {len(self.data[0])} {ele}"

    @classmethod
    def set(cls, data):
        A = cls()
        A.data = data
        return A


def writetc(tc, prefix):
    for id in tc.keys():
        with open(f"{prefix}_tc_{id}.in", "w") as f:
            print(f"## test-case-{id} ##")
            lines = [str(e) for e in tc[id]]
            for line in lines:
                print(line)
                f.write(line + "\n")
